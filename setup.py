import setuptools

with open('README.md', 'r') as fh:
    long_description = fh.read()

setuptools.setup(
    name='rabbitmq-wrapper',
    version='0.0.2',
    author='Khashayar Mirsadeghi',
    author_email='mms.khashayar@gmail.com',
    description='A wrapper for pika to use rabbitmq in python',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/darskakaoo/rabbitmq-wrapper',
    packages=setuptools.find_packages(),
    install_requires=["pika>=1.1.0"],
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    python_requires='>=3.6',
)
