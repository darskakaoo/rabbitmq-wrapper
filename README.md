# Python RabbitMQ Wrapper
This is a wrapper around [pika](https://pypi.org/project/pika/) to connect to RabbitMQ.
There are three singletons provided in this package:

- `Connection`: this singleton is used to create a `BlockingConnection` to rabbitmq. You can pass `host`, `port`, `virtual_host`, `username` and `password` to the constructor for configuration. You can also call `Connection.create_from_django_settings()` which looks in your Django `settings.py` for an object called `RABBITMQ` with a subset of those parameters inside.
**Note** that you should call `connection.connect()` in order to initialize a connection to rabbitmq-server. you can use `connection.disconnect()` when you're done.

- `Publisher`: This singleton is used to create a rabbitmq publisher. There's a method called `publish()` inside which takes `exchange_name`, `queue_name`, `message`, `message_type` and `properties` as parameters. `message_type` is used to determine if the `message` object needs any transformation before publishing. the default value is `json` and it also supports `string` as its value.
**Note** that publishing any message requires initializing a connection first.

- `Consumer`: This singleton is used for consuming exchanges and queues. There two methods available in a consumer: `consume_queue()` and `consume_topic()`.
	- `consume_queue*` takes a `queue_name` and a `callback` as arguments.
	- `consume_topic*` takes an `exchange_name`, a `topic` and a `callback` as parameters.

**Note** that consuming any message requires initializing a connection first.
\* Both these functions are currently **blocking** and prevent the rest of your code from running. You should use them inside async functions if you need more functionality for now.
