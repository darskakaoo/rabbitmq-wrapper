from pika import BlockingConnection, ConnectionParameters, PlainCredentials
from pika.adapters.blocking_connection import BlockingChannel
from pika.exceptions import ConnectionClosed
from .singleton import Singleton


class Connection(metaclass=Singleton):
    _MAX_RETRY_COUNT = 5
    _INSTANCE = None

    @staticmethod
    def create_from_django_settings():
        from django.conf import settings

        rabbit_settings = getattr(settings, 'RABBITMQ', None)

        if rabbit_settings:
            return Connection(**rabbit_settings)

        else:
            raise AssertionError(
                'RABBIT_SETTINGS is not defined in settings.py')

    def __init__(self, host='localhost', port=5672, virtual_host='/', username=None, password=None):
        self._host = host
        self._port = port
        self._virtual_host = virtual_host
        self._username = username
        self._password = password

        self._connection = None  # type: BlockingConnection
        self._channel = None  # type: BlockingChannel
        self._should_be_connected = False

        Connection._INSTANCE = self

    @staticmethod
    def needed(func):

        def run(*args, **kwargs):
            self = Connection._INSTANCE

            if not self._connection:
                raise RuntimeError(f'{func.__name__} requires connection.')

            else:
                successful = False
                retry_count = 0

                while self._should_be_connected and not successful and retry_count < Connection._MAX_RETRY_COUNT:
                    retry_count += 1

                    try:
                        func(*args, **kwargs)
                        successful = True
                    except ConnectionClosed:
                        self.connect()

        return run

    def connect(self):
        options = {
            'host': self._host,
            'port': self._port,
            'virtual_host': self._virtual_host
        }

        if self._username and self._password:
            options['credentials'] = PlainCredentials(
                username=self._username,
                password=self._password,
            )

        self._connection = BlockingConnection(
            ConnectionParameters(**options)
        )

        self._channel = self._connection.channel()
        self._channel.basic_qos(prefetch_count=1)

        self._should_be_connected = True

    def disconnect(self):
        self._should_be_connected = False
        self._connection.close()

    @property
    def channel(self):
        return self._channel
