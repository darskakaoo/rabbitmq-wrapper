from pika import BasicProperties
from json import dumps
from typing import Any
from .singleton import Singleton
from .connection import Connection


class Publisher(metaclass=Singleton):
    def __init__(self):
        self._connection = Connection()
        self._exchanges = []
        self._queues = []

    @staticmethod
    def _format_message(message, message_type):
        if message_type == 'json':
            return dumps(message)

        elif message_type == 'string':
            return message

        else:
            return ''

    @property
    def _channel(self):
        return self._connection.channel

    @Connection.needed
    def publish(self, exchange_name='', queue_name='', message: Any = '', message_type='json', properties=None):
        if not properties:
            properties = {}

        if exchange_name and exchange_name not in self._exchanges:
            self._channel.exchange_declare(
                exchange=exchange_name,
                durable=True,
                exchange_type='topic',
            )

            self._exchanges.append(exchange_name)

        if queue_name and queue_name not in self._queues:
            self._channel.queue_declare(
                queue=queue_name,
                durable=True,
            )

            self._queues.append(queue_name)

        message = Publisher._format_message(message, message_type)

        self._channel.basic_publish(
            exchange=exchange_name,
            routing_key=queue_name,
            body=message,
            properties=BasicProperties(
                delivery_mode=2,
                **properties,
            ),
        )
