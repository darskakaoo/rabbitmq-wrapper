from .singleton import Singleton
from .connection import Connection


class Consumer(metaclass=Singleton):

    def __init__(self):
        self._connection = Connection()
        self._exchanges = []
        self._queues = []

        self._consuming_started = False

    @property
    def _channel(self):
        return self._connection.channel

    @Connection.needed
    def _consume(self, *args, **kwargs):
        self._channel.basic_consume(*args, **kwargs, auto_ack=True)

        if not self._consuming_started:
            self._consuming_started = True
            self._channel.start_consuming()

    @Connection.needed
    def consume_topic(self, exchange_name, topic, callback):
        if exchange_name not in self._exchanges:
            self._channel.exchange_declare(
                exchange=exchange_name,
                durable=True,
                exchange_type='topic',
            )

            self._exchanges.append(exchange_name)

            queue_name = self._channel.queue_declare(
                queue='',
                exclusive=True
            ).method.name

            self._channel.queue_bind(
                queue=queue_name,
                exchange=exchange_name,
                routing_key=topic
            )

            self._consume(
                queue=queue_name,
                on_message_callback=callback,
            )

    @Connection.needed
    def consume_queue(self, queue_name, callback):
        if queue_name not in self._queues:
            self._channel.queue_declare(
                queue=queue_name,
                durable=True,
            )

            self._queues.append(queue_name)

            self._consume(
                queue=queue_name,
                on_message_callback=callback,
            )
